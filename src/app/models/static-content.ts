export interface StaticContent {
    heading: string;
    summary: string;
    body: string;
}
