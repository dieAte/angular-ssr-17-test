import { Routes } from '@angular/router';
import { HomeComponent } from "./components/home/home.component";
import { StaticContentComponent } from "./components/static-content/static-content.component";
import { WeatherComponent } from "./components/weather/weather.component";

export const routes: Routes = [
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'weather',
        component: WeatherComponent
    },
    {
        path: 'static-content/:id',
        component: StaticContentComponent
    },
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    }
];
