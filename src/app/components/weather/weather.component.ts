import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import {
    afterNextRender,
    Component
} from '@angular/core';
import { WeatherForCastService } from "../../services/weather-for-cast.service";

@Component({
    selector: 'app-wether',
    standalone: true,
    imports: [
        CommonModule,
        HttpClientModule
    ],
    templateUrl: './weather.component.html',
    styleUrl: './weather.component.scss'
})
export class WeatherComponent {
    weatherforcast: any;

    constructor(
        private weatherHandler: WeatherForCastService
    ) {
        //  Weather forecast shall NOT be rendered on server but on client
        afterNextRender(() => {
            this.weatherHandler.getWeatherforcast().subscribe((data) => {
                this.weatherforcast = data;
            });
        });
    }
}
