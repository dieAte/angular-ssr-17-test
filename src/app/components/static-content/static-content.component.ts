import { CommonModule } from "@angular/common";
import {
    HttpClient,
    HttpClientModule
} from "@angular/common/http";
import {
    Component,
    OnInit
} from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { StaticContent } from "../../models/static-content";
import { STATIC_CONTENT } from "../../static-content-configs/content";

@Component({
    selector: 'app-static-content',
    standalone: true,
    imports: [
        CommonModule,
        HttpClientModule
    ],
    templateUrl: './static-content.component.html',
    styleUrl: './static-content.component.scss'
})
export class StaticContentComponent implements OnInit {
    content: StaticContent | undefined;

    constructor(private route: ActivatedRoute) { }

    ngOnInit(): void {
        console.log("Static Content Component Initialized")
        this.route.params.subscribe(params => {
            const id = params['id']; // Access the 'id' route parameter
            this.content = STATIC_CONTENT[id];
            console.log("Static Content Component generated for ", id);
        });
    }
}
