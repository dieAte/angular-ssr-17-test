import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import {
  Component,
  inject,
  OnInit
} from '@angular/core';
import { PostHandlerService } from "../../services/post-handler.service";

@Component({
  selector: 'app-posts',
  standalone: true,
  imports: [
      CommonModule,
      HttpClientModule
  ],
  templateUrl: './posts.component.html',
  styleUrl: './posts.component.scss'
})
export class PostsComponent implements OnInit {
  posts: any;
  postsHandler = inject(PostHandlerService);
  ngOnInit(): void {
    // posts are automatically fetched on server
    this.postsHandler.getPosts().subscribe((data) => {
      this.posts = data;
    });
  }

}
