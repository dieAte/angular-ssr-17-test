import {
  afterNextRender,
  Component,
  inject,
  PLATFORM_ID
} from '@angular/core';
import { CookieService } from "ngx-cookie-service";
import { PostsComponent } from "../posts/posts.component";

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    PostsComponent
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {
  platformId = inject(PLATFORM_ID);
  constructor(
      private cookieService: CookieService
  ) {
    // console log will log:
    // PlatformId: server   ------ on Node server console
    // PlatformId: browser  ------ inside browser console
    console.log(`PlatformId: ${this.platformId}`);

    // cookies are only available in browser ->
    // set/get cookie only during client side rendering -> inside afterNextRender
    afterNextRender(() => {
      if (!cookieService.get("SSRTestToken")) {
        cookieService.set("SSRTestToken", "I'm the SSRTestToken!!");
      }
      console.log("SSRTestToken: ", cookieService.get("SSRTestToken"));
    })
  }
}
