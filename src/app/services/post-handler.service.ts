import { HttpClient } from "@angular/common/http";
import {
  inject,
  Injectable
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PostHandlerService {
  private apiUrl = 'https://jsonplaceholder.typicode.com/posts'
  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get(this.apiUrl);
  }
}
